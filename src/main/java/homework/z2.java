package homework;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class z2 {
    //metoda statycznie dostępna dla wszystkich z nazwą ex2/typzwracany
    public static Set<String> ex2(){
        //zbiór tekstów <typ jaki przyjmuje> nazwa = nowy obiekt typu hashset
        Set<String> setExamples = new HashSet<>();
        setExamples.add("Adam");
        setExamples.add("Anna");
        setExamples.add("Agata");

        System.out.println("Size: " + setExamples.size());
        setExamples.add("Magda");
        System.out.println("Size: " + setExamples.size());
        //przez return zwraca wartość ta metoda
        return setExamples;
    }

    public static void main(String[] args) {
        //et<String> returnSetExamples = setExamples
        Set<String> returnSetExamples = ex2();
        for (String name: returnSetExamples){
            System.out.println("Hello " + name);
        }

    }

}
