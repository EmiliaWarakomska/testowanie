package sda_mz;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Address address = new Address("Kwiatowa", "Warszawa", "Poland", 50, 87);
        Person young = new Person();
        young.setAddress(address);
        Person johny = new Person("Johny", "Bravo");
        Person ana = new Person("Ana", "Nowak", 25);
        Person old = new Person("old", "Nowak", 135);
        johny.setAddress(address);
        ana.setAddress(address);
        old.setAddress(address);


        System.out.println(young.getAge());
        young.setAge(21);
        System.out.println(young.getAge());


        List<Person> listOfPerson = new ArrayList<>();
        listOfPerson.add(young);
        listOfPerson.add(johny);
        listOfPerson.add(ana);
        listOfPerson.add(old);


        System.out.println("size: " + listOfPerson.size());

        if (!listOfPerson.isEmpty()) {
            Person person = listOfPerson.get(0);
            System.out.println("index 0: " + person);
            System.out.println("index last: " + listOfPerson.get(listOfPerson.size() - 1));

        }
        System.out.println("Drukujemy wszystkich");

        for (Person person : listOfPerson) {
            System.out.println("person " + person.toString());
        }
        System.out.println("Drukujemy przefiltrowane");
        List<Person> filtered = Validator.filterPerson(listOfPerson);

        for (Person person : filtered) {
            System.out.println("person " + person.toString());
        }
    }
}
