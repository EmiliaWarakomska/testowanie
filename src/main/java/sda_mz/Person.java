package sda_mz;

//import lombok.AllArgsConstructor;
//import lombok.Getter;
//import lombok.Setter;
//
//@Getter
//@Setter
//@AllArgsConstructor - odpowiada konstruktorowi ze wszystkimi parametrami
public class Person {
    public static final Integer MAX_AGE = 130;

    // pola lub atrybuty - fielsd or atributs

    private String name;
    private String surname;
    private int age;
    private Address address;


    //konstruktor z parametrami
    public Person(){
        this.name = "młody";
        this.surname = "buk";
        this.age = 19;
    }

    public Person(String name, String surname){
        this.name = name;
        this.surname = surname;
        this.age = 20;
    }

    public Person(String name, String surname, Integer age){
        this.name = name;
        this.surname = surname;
        this.age = age;

    }

    public int getAge(){
        return this.age;
    }

    public void setAge(int wiek){
        this.age = wiek;

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public void introduce(){
        System.out.println("Hello I'm " + this.name + " " + this.surname);
    }

    public void introduce(String hello){
        System.out.println(hello + " " + this.name + " " + this.surname);
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                ", address=" + address.toString() +
                '}';
    }
}

